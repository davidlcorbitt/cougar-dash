This app should reduce the amount of time for which I have to wait in line at the CougarEat.

CougarDashApp is the React Native frontend mobile app

to run,

1. `yarn install`
2. `yarn start`

cougar-dash is the ruby on rails backend server

to run,

1. `bundle install`
2. `rails server`
