import React from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { StackActions, NavigationActions } from "react-navigation";

interface MyProps {
  title: string;
  location: string;
  logo: any;
  navigation: any;
  theme: any;
}

export default class CommonRequest extends React.Component<MyProps, MyProps> {
  componentDidMount() {
    const { title, location, logo, navigation } = this.props;
    const pushAction = StackActions.push({
      routeName: "AddOrder",
      params: {
        storeName: title,
        storeLogo: logo,
        title: "Hello"
      }
    });
    navigation.dispatch(pushAction);
  }

  render() {
    if (!this.props) {
      return null;
    }
    const { title, location, logo, navigation } = this.props;

    const pushAction = StackActions.push({
      routeName: "AddOrder",
      params: {
        storeName: title,
        storeLogo: logo,
        title: "Hello"
      }
    });

    return (
      <TouchableOpacity
        onPress={() => {
          navigation.dispatch(pushAction);
        }}
        style={{
          ...styles.request,
          // backgroundColor: this.props.theme.dark
          //   ? this.props.theme.colors.placeholder
          //   : "#e8e8e8"
          backgroundColor: "#e8e8e8"
        }}
      >
        <Image style={styles.logo} source={logo} />
        <View style={{ flex: 1, paddingLeft: 30, paddingRight: 30 }}>
          <Text style={{ fontSize: 22 }}>{title}</Text>
          <Text style={{ fontSize: 15 }}>{location}</Text>
        </View>

        <View />
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  request: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "80%",
    backgroundColor: "#f8f8f8",
    height: 80,
    alignItems: "center",
    padding: 10,
    borderRadius: 10,
    marginBottom: 15
  },
  logo: {
    width: 40,
    height: 40,
    marginLeft: 10,
    borderRadius: 5
  }
});
