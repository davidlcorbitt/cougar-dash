import React from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  FlatList,
  SafeAreaView
} from "react-native";

import {
  TextField,
  FilledTextField,
  OutlinedTextField
} from "react-native-material-textfield";

import NumericInput from "react-native-numeric-input";

interface MyProps {
  item: any;
  updateItem: any;
  items: Array<any>;
  theme: any;
}

interface MyState {
  showList: boolean;
}

export default class OrderItem extends React.Component<MyProps, MyState> {
  state: MyState = { showList: false };
  topY = 0;
  middleY = 0;
  bottomY = 0;
  width = 0;

  componentDidMount() {
    this.setState({ showList: false });
  }

  render() {
    if (!this.props) {
      return null;
    }
    const { item, updateItem, items, theme } = this.props;
    const listY = this.topY + this.middleY + this.bottomY;
    console.log(theme);

    return (
      <View
        style={{
          height: 40,
          justifyContent: "space-around",
          flexDirection: "row",
          alignItems: "center"
        }}
        onLayout={event => {
          const layout = event.nativeEvent.layout;
          this.topY = layout.y;
        }}
      >
        <View
          style={{
            width: 80,
            flex: 3,
            paddingLeft: 40
          }}
          onLayout={event => {
            const layout = event.nativeEvent.layout;
            this.middleY = layout.y;
          }}
        >
          <TextField
            placeholder="Next Item"
            value={item.name}
            onChange={event =>
              updateItem({
                name: event.nativeEvent.text,
                quantity: item.quantity
              })
            }
            onFocus={() => {
              this.setState({ showList: true });
              this.forceUpdate();
            }}
            onBlur={() => this.setState({ showList: false })}
            onLayout={event => {
              const layout = event.nativeEvent.layout;
              this.bottomY = layout.y;
              this.width = layout.width;
            }}
            baseColor={theme.colors.primary}
            tintColor={theme.colors.primary}
            animationDuration={0}
          />
          {this.state.showList && (
            <SafeAreaView
              style={{
                borderColor: theme.colors.primary,
                position: "absolute",
                borderWidth: 1,
                left: 40,
                top: this.topY + this.middleY + this.bottomY,
                width: this.width
              }}
            >
              <FlatList
                data={items}
                renderItem={({ item }) => (
                  <View
                    style={{
                      justifyContent: "space-between",
                      flexDirection: "row",
                      backgroundColor: "rgba(7, 0, 159, .08)"
                    }}
                  >
                    <Text>{item.name}</Text>
                    <Text>{item.price}</Text>
                  </View>
                )}
                ItemSeparatorComponent={() => (
                  <View
                    style={{ height: 5, backgroundColor: theme.colors.primary }}
                  />
                )}
                keyExtractor={item => item.id}
              />
            </SafeAreaView>
          )}
        </View>

        <View
          style={{
            flex: 1,
            height: 60,
            justifyContent: "flex-end",
            alignItems: "flex-start",
            marginLeft: 10
          }}
        >
          <NumericInput
            value={item.quantity}
            onChange={value => {
              updateItem({ name: item.name, quantity: value });
            }}
            minValue={0}
            totalHeight={40}
            totalWidth={70}
            rounded
            borderColor="white"
            iconStyle={{ color: "white" }}
            rightButtonBackgroundColor="#00929f"
            leftButtonBackgroundColor="#00929f"
            // type="up-down"
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  request: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "80%",
    backgroundColor: "#f8f8f8",
    height: 80,
    alignItems: "center",
    padding: 10,
    borderRadius: 10,
    marginBottom: 15
  },
  logo: {
    width: 40,
    height: 40,
    marginLeft: 10,
    borderRadius: 5
  }
});
