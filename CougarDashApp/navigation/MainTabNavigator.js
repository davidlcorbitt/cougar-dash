import React from "react";
import { Platform, TouchableOpacity, Image } from "react-native";
import {
  createStackNavigator,
  createBottomTabNavigator
} from "react-navigation";

import { TabBarIcon } from "../components";

import {
  SettingsScreen,
  JobsScreen,
  OrdersScreen,
  AddOrderScreen
} from "../screens";

const config = Platform.select({
  web: { headerMode: "screen" }
});

const baseNavigation = {
  headerStyle: {
    backgroundColor: "#07009F",
    height: 50,
    borderWidth: 0,
    borderBottomColor: "transparent",
    shadowColor: "transparent",
    elevation: 0
  },
  headerTintColor: "#fff",
  headerTitleStyle: {
    fontWeight: "bold"
  },
  headerLeft: (
    <TouchableOpacity onPress={() => alert("hello world!")}>
      <Image
        style={{ width: 40, height: 40, borderRadius: 20, marginLeft: 10 }}
        source={require("../assets/images/user-image.png")}
      />
    </TouchableOpacity>
  )
};

higherNavigation = {
  headerStyle: {
    backgroundColor: "#07009F",
    height: 50
  },
  headerTintColor: "#fff",
  headerTitleStyle: {
    fontWeight: "bold"
  }
};

const JobsStack = createStackNavigator(
  {
    Home: JobsScreen
  },
  config
);

JobsScreen.navigationOptions = baseNavigation;

JobsStack.navigationOptions = {
  tabBarLabel: "Jobs",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === "ios" ? "md-checkmark" : "md-checkmark"}
    />
  )
};

JobsStack.path = "";

const OrdersStack = createStackNavigator(
  {
    Orders: OrdersScreen,
    AddOrder: AddOrderScreen
  },
  config
);

OrdersScreen.navigationOptions = baseNavigation;
AddOrderScreen.defaultNavigationOptions = higherNavigation;

OrdersStack.navigationOptions = {
  tabBarLabel: "Order",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === "ios" ? "md-add" : "md-add"}
    />
  )
};

OrdersStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
    tabBarLabel: "Order",
    tabBarIcon: ({ focused }) => (
      <TabBarIcon
        focused={focused}
        name={Platform.OS === "ios" ? "md-add" : "md-add"}
      />
    )
  };
};

OrdersStack.path = "";

const SettingsStack = createStackNavigator(
  {
    Settings: SettingsScreen
  },
  config
);

SettingsScreen.navigationOptions = baseNavigation;

SettingsStack.navigationOptions = {
  tabBarLabel: "Settings",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === "ios" ? "ios-options" : "md-options"}
    />
  )
};

SettingsStack.path = "";

const tabNavigator = createBottomTabNavigator(
  {
    JobsStack,
    OrdersStack,
    SettingsStack
  },
  {
    initialRouteName: "OrdersStack"
  }
);

tabNavigator.path = "";

export default tabNavigator;
