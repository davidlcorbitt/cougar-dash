import { AppLoading } from "expo";
import { Asset } from "expo-asset";
import * as Font from "expo-font";
import React, { useState } from "react";
import { Platform, StatusBar, StyleSheet, View } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import {
  DefaultTheme,
  DarkTheme,
  Provider as PaperProvider
} from "react-native-paper";
import {
  AppearanceProvider,
  useColorScheme,
  Appearance
} from "react-native-appearance";

import AppNavigator from "./navigation/AppNavigator";

let darkTheme = {
  ...DarkTheme,
  colors: {
    ...DarkTheme.colors,
    background: "#49494B"
  }
};

export default function App(props) {
  const [isLoadingComplete, setLoadingComplete] = useState(false);
  let colorScheme = useColorScheme();
  let defaultTheme = DefaultTheme;
  let theme = {
    ...defaultTheme,
    roundness: 2,
    colors: {
      ...defaultTheme.colors,
      primary: "#07009F",
      accent: "#f1c40f"
    }
  };
  theme.dark = colorScheme === "dark";

  let subscription = Appearance.addChangeListener(({ colorScheme }) => {
    // do something with color scheme
    console.log(colorScheme);
    console.log("is the new color scheme");
    theme.dark = colorScheme === "dark";
  });

  if (!isLoadingComplete && !props.skipLoadingScreen) {
    return (
      // <AppearanceProvider>
      <AppLoading
        startAsync={loadResourcesAsync}
        onError={handleLoadingError}
        onFinish={() => handleFinishLoading(setLoadingComplete)}
      />
      // </AppearanceProvider>
    );
  } else {
    return (
      // <AppearanceProvider>
      <View style={styles.container}>
        <PaperProvider theme={theme}>
          {Platform.OS === "ios" && <StatusBar barStyle="default" />}
          <AppNavigator />
        </PaperProvider>
      </View>
      // </AppearanceProvider>
    );
  }
}

async function loadResourcesAsync() {
  await Promise.all([
    Asset.loadAsync([
      require("./assets/images/robot-dev.png"),
      require("./assets/images/robot-prod.png")
    ]),
    Font.loadAsync({
      // This is the font that we are using for our tab bar
      ...Ionicons.font,
      // We include SpaceMono because we use it in HomeScreen.js. Feel free to
      // remove this if you are not using it in your app
      "space-mono": require("./assets/fonts/SpaceMono-Regular.ttf")
    })
  ]);
}

function handleLoadingError(error) {
  // In this case, you might want to report the error to your error reporting
  // service, for example Sentry
  console.warn(error);
}

function handleFinishLoading(setLoadingComplete) {
  setLoadingComplete(true);
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
