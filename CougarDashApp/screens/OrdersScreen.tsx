import React from "react";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import { CommonRequest } from "../components";
import { withTheme } from "react-native-paper";

interface MyProps {
  theme: any;
  navigation: any;
}

class OrdersScreen extends React.Component<MyProps> {
  render() {
    return (
      <View
        style={{
          ...styles.container,
          backgroundColor: this.props.theme.colors.background
        }}
      >
        <ScrollView
          style={styles.container}
          contentContainerStyle={styles.contentContainer}
        >
          <View>
            <Text
              style={{
                ...styles.title,
                color: this.props.theme.colors.onBackground
              }}
            >
              Common Orders
            </Text>
            <CommonRequest
              title="Taco Bell"
              location="Cougareat"
              logo={require("../assets/images/taco-bell.png")}
              navigation={this.props.navigation}
              theme={this.props.theme}
            />
            <CommonRequest
              title="Wendy's"
              location="Cougareat"
              logo={require("../assets/images/wendys.jpeg")}
              navigation={this.props.navigation}
              theme={this.props.theme}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  contentContainer: {
    flex: 1,
    alignItems: "center",
    marginTop: 20,
    justifyContent: "flex-start",
    width: "100%"
  },
  request: {
    fontSize: 12,
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "#f8f8f8",
    width: "100%",
    height: 80,
    alignItems: "center",
    padding: 10,
    borderRadius: 10
  },
  logo: {
    width: 40,
    height: 40,
    marginLeft: 10,
    borderRadius: 5
  },
  title: {
    fontSize: 25,
    paddingBottom: 10
  }
});

export default withTheme(OrdersScreen);
