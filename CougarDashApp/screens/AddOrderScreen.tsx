import React from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity
} from "react-native";
import { withTheme } from "react-native-paper";
import {
  TextField,
  FilledTextField,
  OutlinedTextField
} from "react-native-material-textfield";
import DateTimePicker from "react-native-modal-datetime-picker";
import Moment from "moment";
import {
  Appearance,
  useColorScheme,
  AppearanceProvider
} from "react-native-appearance";

import { OrderItem } from "../components";
import { CheckBox } from "react-native-elements";

interface MyProps {
  theme: any;
  navigation: any;
}

interface MyState {
  items: Array<any>;
  details: string;
  time: Date;
  isDateTimePickerVisible: boolean;
  colorScheme: string;
  asap: boolean;
}

class AddOrderScreen extends React.Component<MyProps, MyState> {
  state: MyState = {
    items: [{ name: "Beefy Frito Burrito", quantity: 0 }],
    details: "",
    time: new Date(),
    isDateTimePickerVisible: false,
    colorScheme: Appearance.getColorScheme(),
    asap: true
  };

  items = [
    { name: "Beefy Frito Burrito", price: 1.5, key: "1", id: "1" },
    { name: "Cheesy Bean and Rice Burrito", price: 1.5, key: "2", id: "2" }
  ];

  componentDidMount() {
    Moment.locale("en");
    this.setState({
      items: [{ name: "Beefy Frito Burrito", quantity: 0 }],
      details: "",
      time: new Date(),
      isDateTimePickerVisible: false,
      colorScheme: Appearance.getColorScheme(),
      asap: true
    });
  }

  showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true });
  };

  hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
  };

  handleTimePicked = time => {
    this.setState({ time });
    this.hideDateTimePicker();
  };

  render() {
    if (!this.props) {
      return null;
    }
    return (
      <View
        style={{
          ...styles.container,
          backgroundColor: this.props.theme.colors.background
        }}
      >
        <ScrollView
          style={styles.container}
          contentContainerStyle={styles.contentContainer}
        >
          <View style={{ flex: 1, width: "90%" }}>
            <View style={{ marginBottom: 100, flexDirection: "column" }}>
              <Text
                style={{
                  ...styles.sectionLabel,
                  marginBottom: 10
                }}
              >
                Order
              </Text>
              <OrderItem
                item={this.state.items[0]}
                updateItem={item => {
                  let items = this.state.items;
                  items[0] = item;
                  this.setState({ items });
                }}
                items={this.items}
                theme={this.props.theme}
              />
            </View>
            <View style={{ paddingTop: 30 }}>
              <OutlinedTextField
                multiline={true}
                label="Custom Instructions"
                value={this.state.details}
              />
            </View>
            <View
              style={{
                flexDirection: "column",
                justifyContent: "space-around",
                alignItems: "flex-start",
                paddingTop: 30
              }}
            >
              <View>
                <Text style={{ fontSize: 20 }}>Pickup Time</Text>
              </View>

              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-around"
                }}
              >
                <CheckBox
                  title="ASAP"
                  checked={this.state.asap}
                  onPress={() => this.setState({ asap: !this.state.asap })}
                  checkedColor={this.props.theme.colors.primary}
                />
                <TouchableOpacity
                  onPress={() => this.showDateTimePicker()}
                  disabled={this.state.asap}
                  style={
                    this.state.asap ? styles.disabledTime : styles.enabledTime
                  }
                >
                  <Text style={{ fontSize: 19 }}>
                    {Moment(this.state.time).format("hh:mm")}
                  </Text>
                  <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this.handleTimePicked}
                    onCancel={this.hideDateTimePicker}
                    date={this.state.time}
                    mode="time"
                    titleIOS="Pick a time"
                    isDarkModeEnabled={this.props.theme.dark}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerStyle: {
        backgroundColor: "#07009F",
        height: 50
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold"
      },

      headerTitle: (
        <View
          style={{
            display: "flex",
            justifyContent: "center",
            flexDirection: "row",
            alignItems: "center"
          }}
        >
          <Text style={{ color: "white", fontSize: 20 }}>
            {navigation.getParam("storeName")}
          </Text>
          <Image
            style={styles.logo}
            source={navigation.getParam("storeLogo")}
          />
        </View>
      )
    };
  };
}

const timeWrapper = {
  padding: 10,
  borderRadius: 5,
  backgroundColor: "#d8d8f8"
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  contentContainer: {
    flex: 1,
    alignItems: "center",
    margin: 20,
    justifyContent: "flex-start"
  },
  logo: {
    width: 30,
    height: 30,
    marginLeft: 10,
    borderRadius: 5
  },
  sectionLabel: {
    fontSize: 26,
    marginTop: 20
  },
  disabledTime: {
    ...timeWrapper,
    backgroundColor: "#7c7c7c"
  },
  enabledTime: {
    ...timeWrapper
  }
});

export default withTheme(AddOrderScreen);
